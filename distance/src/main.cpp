/**
 * Identifiant :Alavoine Antoine
 * Date :18/02/2021
 * environement :nodemcuv2
 * platform = espressif8266 board = nodemcuv2  
 * framework = arduino
 * */

#include <Arduino.h>
#include <Wire.h>
#include "SFR02.hpp"


int distance = 0,
    sda = D2,
    scl = D1,
    addressI2C = 0x70;
    result ;

SRF02.objtest(addressI2C, sca, scl);

void setup()
{
    Wire.begin(SDA, SCL);       //initialisation de l'afficheur OLED
    Serial.begin(9600); // start serial communication at 9600bps
}
void loop()
{
    result = objtest.LireDistance();
    delay(500); // wait a bit since people have to read the output :)
}
