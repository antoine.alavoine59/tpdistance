#pragma once 
#include <Wire.h>
#include <Arduino.h>

class SFR02
{

private:
    int reading,
        sda,
        scl,
        addressI2C;

public:
    SFR02(uint8_t _addressI2C, uint8_t _sda, uint8_t _scl);
    ~SFR02();
    int LireDistance();
};